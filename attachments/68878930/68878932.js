'use strict';

const common = require('aveva.connect.services.common'),
    acmCertificate = require("@aveva/acm-certificate"),
    Constants = require('../common/Constants'),
    Response = require('../common/Response');

class Certificate {
    constructor(event, context) {
        this._event = event || {};
        this._context = context || {};

        this._log = new common.logging.DiagnosticLog(Constants.SOURCE);
    }

    run() {
        var contact = this._event.ResourceProperties.Contact;
        var domainName = this._event.ResourceProperties.DomainName;
        var region = this._event.ResourceProperties.Region;

        if (this._event.RequestType === "Create" || this._event.RequestType === "Update") {
            return acmCertificate.selectCertificate({
                domainName: domainName,
                region: region,
                requestCertificateOptions: {
                    route53zoneId: "auto",
                    contact: contact
                }
            })
                .then(cert => {
                    return Response.send(this._event, this._context, "SUCCESS", { CertificateArn: cert, DomainName: domainName }, cert, `Created certificate for ${domainName}`);
                })
                .catch(err => {
                    console.log(JSON.stringify(err));
                    return Response.send(this._event, this._context, "FAILED", null, domainName, err.message);
                });
        } else if (this._event.RequestType === "Delete") {
            return Response.send(this._event, this._context, "SUCCESS", null, this._event.PhysicalResourceId, `Deleted certificate for ${domainName}`);
        }
        return Response.send(this._event, this._context, "FAILED", null, this._event.PhysicalResourceId, `Unsupported event request type ${this._event.RequestType}`);
    }
}

module.exports = Certificate;